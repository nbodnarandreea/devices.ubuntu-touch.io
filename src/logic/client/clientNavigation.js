var availableContainers, currentPath;

export function navigate(link, isPop) {
  if (new URL(link).pathname.replace(/\/+$/, "") == currentPath) return;
  // Import next page
  fetch(link)
    .then((response) => response.text())
    .then((data) => {
      const doc = new DOMParser().parseFromString(data, "text/html");
      let container = getContainers(
        getContainers(availableContainers, doc),
        document
      )[0];
      document
        .querySelector(container)
        .replaceWith(doc.querySelector(container));
      document.head.replaceWith(doc.head);

      // Execute all scripts ( see Astro SPA )
      Promise.all(
        [
          ...document.querySelectorAll("head script, " + container + " script")
        ].map((script) => {
          return new Promise((resolve) => {
            const newScript = document.createElement("script");
            newScript.text = script.text;
            for (const attr of script.attributes) {
              newScript.setAttribute(attr.name, attr.value);
            }
            newScript.addEventListener("load", () => {
              resolve(newScript.src);
            });
            script.replaceWith(newScript);
          });
        })
      ).then(() => {
        if (!isPop) history.pushState({}, "", link);
        currentPath = new URL(link).pathname.replace(/\/+$/, "");
        window.dispatchEvent(new Event("load"));
        window.dispatchEvent(new Event("spa:navigation"));
      });
    });
}

export function initNavigation(preferredContainers) {
  // Set custom client navigation
  availableContainers = preferredContainers;
  currentPath = window.location.pathname.replace(/\/+$/, "");

  // Get preferred container and push content without changing state
  addEventListener("popstate", (event) => {
    navigate(window.location, true);
  });

  window.addEventListener("load", () => {
    [...document.links]
      .filter((link) => isInternalLink(link))
      .forEach((link) => {
        link.onclick = (e) => {
          e.preventDefault();
          navigate(link.href);
        };
      });
  });
}

export function componentScriptLoader(container, componentScript) {
  console.log("registering " + container);

  window.addEventListener("load", () => {
    let component = document.querySelector(container);
    if (!!component && !component.classList.contains("script-registered")) {
      console.log("running " + container);
      componentScript();
      component.classList.add("script-registered");
    }
  });
}

function isInternalLink(link) {
  return (
    link.host == window.location.host &&
    !(link.pathname == window.location.pathname && link.hash) &&
    link.dataset.ignoreNavigation !== ""
  );
}

function getContainers(containers, doc) {
  return containers.filter((container) => !!doc.querySelector(container));
}

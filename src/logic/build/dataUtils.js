import portStatus from "@data/portStatus.json";

/* Helpers */

// Get category from collection
export function getCategoryByName(device, category) {
  return device.portStatus.find((el) => el.categoryName == category);
}

// Get feature from category
export function getFeatureById(category, featureId) {
  return category.features.find((el) => el.id == featureId);
}

/* Run a function for each feature
 * The function will be called with
 * the feature and the category of
 * the feature.
 */
export function forEachFeature(device, fn) {
  for (let portCategory in portStatus) {
    // Get category from collection
    let category = getCategoryByName(device, portCategory);

    if (category) {
      for (let feature of portStatus[portCategory]) {
        fn(feature, category);
      }
    }
  }
}

// Get device specification from deviceInfo
export function getSpecificationById(device, spec) {
  return device.deviceInfo.find((el) => el.id == spec);
}

// Conversion functions for portStatus
export function portStatusAsObject(portStatus) {
  if (!portStatus) return undefined;
  return Object.fromEntries(
    portStatus.map((category) => [
      category.categoryName,
      Object.fromEntries(
        category.features.map((feature) => {
          let featureId = feature.id;
          delete feature.id;
          return [featureId, feature];
        })
      )
    ])
  );
}

export function portStatusAsArray(portStatus) {
  if (!portStatus) return undefined;
  return Object.entries(portStatus).map((category) => {
    return {
      categoryName: category[0],
      features: Object.entries(category[1]).map((feature) => {
        return {
          id: feature[0],
          ...feature[1]
        };
      })
    };
  });
}

// Conversion functions for deviceInfo
export function deviceInfoAsObject(deviceInfo) {
  if (!deviceInfo) return undefined;
  return Object.fromEntries(deviceInfo.map((spec) => [spec.id, spec.value]));
}

export function deviceInfoAsArray(deviceInfo) {
  if (!deviceInfo) return undefined;
  return Object.entries(deviceInfo).map((spec) => {
    return {
      id: spec[0],
      value: spec[1]
    };
  });
}

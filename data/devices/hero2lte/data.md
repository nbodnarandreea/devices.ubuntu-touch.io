---
variantOf: "herolte"
name: "Samsung Galaxy S7 Edge (Exynos)"

deviceInfo:
  - id: "battery"
    value: "3600 mAh"
  - id: "display"
    value: "1440x2560 pixels, 5.5 in"
  - id: "dimensions"
    value: "150.9 mm (5.9 in) x 72.6 mm (2.9 in) x 7.7 mm (0.3 in)"
  - id: "weight"
    value: "157 g"
---

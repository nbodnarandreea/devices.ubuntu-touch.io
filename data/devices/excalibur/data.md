---
variantOf: "gram"
name: "Xiaomi Redmi Note 9 Pro Max"

deviceInfo:
  - id: "ram"
    value: "6GB/8GB"
  - id: "display"
    value: "IPS LCD, 450 nits (typ), 6.67 inches, 107.4 cm2 (~84.5% screen-to-body ratio) 1080 x 2400 pixels, 20:9 ratio (~395 ppi density)"
  - id: "rearCamera"
    value: "64MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
---

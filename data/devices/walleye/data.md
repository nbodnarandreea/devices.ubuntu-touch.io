---
variantOf: "taimen"
name: "Google Pixel 2"

deviceInfo:
  - id: "battery"
    value: "2700 mAh"
  - id: "display"
    value: "1920x1080 5in"
  - id: "dimensions"
    value: "145.7mm (5.74in) x 69.7mm (2.74in) x 7.8mm (0.31in)"
  - id: "weight"
    value: "143 g (5.04 oz)"
---

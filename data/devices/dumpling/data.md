---
variantOf: "cheeseburger"
name: "Oneplus 5T"

deviceInfo:
  - id: "display"
    value: "1080x2160 pixels, 6 in"
  - id: "dimensions"
    value: "1156.1x75x7.3 mm"
  - id: "weight"
    value: "162 g"
  - id: "releaseDate"
    value: "21.11.2017"
---

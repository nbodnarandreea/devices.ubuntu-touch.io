---
name: "Raspberry Pi 3"
comment: "experimental"
deviceType: "tv"
buyLink: "https://www.raspberrypi.org/products/"
description: "Raspberry Pi 3B+ and a good power supply, Micro SD card (8-32GB), card reader in desktop or laptop computer (or a Raspberry Pi running Linux), USB keyboard and mouse, HDMI monitor/TV and cable, Ethernet connection and cable. The Raspberry Pi is an affordable ARM-based linux single-board computer. An experimental Ubuntu Touch image is available."
subforum: "43/ut-for-raspberry-pi"
price:
  avg: 35

deviceInfo:
  - id: "cpu"
    value: "Quad core Cortex-A53 64 bit"
  - id: "chipset"
    value: "Broadcom BCM2837"
  - id: "gpu"
    value: "Broadcom VideoCore IV"
  - id: "rom"
    value: "Any microSD card"
  - id: "ram"
    value: "1GB"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "86.9mm x 58.5mm x 19.1mm"
  - id: "weight"
    value: "41.2g"

docLinks:
  - name: "How to install and help to test."
    link: "https://ubports.com/blog/ubports-news-1/post/raspberry-pi-114"

communityHelp:
  - name: "Telegram - @UBports_pi"
    link: "https://t.me/UBports_pi"

externalLinks:
  - name: "Git Lab Repository"
    link: "https://gitlab.com/ubports/community-ports/raspberrypi"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/raspberrypi/-/issues"
---

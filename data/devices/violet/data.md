---
name: "Xiaomi Redmi Note 7 Pro"
description: "A powerful device with 8 cores, up to 6 GB of RAM, a 48MP camera. It's only waiting for you to install Ubuntu Touch on it!"
price:
  avg: 200
comment: "community device"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm SDM675 Snapdragon 675"
  - id: "arch"
    value: "arm64"
  - id: "gpu"
    value: "Qualcomm Adreno 612"
  - id: "rom"
    value: "64/128GB"
  - id: "ram"
    value: "4/6GB"
  - id: "android"
    value: "Android 9.0"
  - id: "battery"
    value: "4000 mAh"
  - id: "display"
    value: "2340x1080 pixels, 6.3 in"
  - id: "rearCamera"
    value: "48MP"
  - id: "frontCamera"
    value: "13MP"
  - id: "dimensions"
    value: "159.21 mm x 75.21 mm x 8.1 mm"
  - id: "weight"
    value: "186 g"
contributors:
  - name: "Alberto Mardegan"
    photo: "https://secure.gravatar.com/avatar/2166d155925e8efa7b4b7f9849064724"
    forum: "https://forums.ubports.com/user/mardy"

communityHelp:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
  - name: "Xiaomi devices subforum"
    link: "https://forums.ubports.com/category/91/xiaomi"
externalLinks:
  - name: "Progress reports"
    link: "http://www.mardy.it/categories/ubports.html"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/android9/xiaomi-redmi-note-7-pro/xiaomi-violet/-/issues"
---

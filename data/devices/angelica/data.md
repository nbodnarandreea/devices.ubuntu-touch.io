---
name: "Xiaomi Redmi 9C"
deviceType: "phone"
description: "Perfect phone to test out UT. Cheapily available in almost any country and the competitor for the Nexus 5 in price "
image: "https://fdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-9c-1.jpg"
price:
  min: 110
  max: 275

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "MediaTek Helio G35"
  - id: "gpu"
    value: "PowerVR GE8320"
  - id: "rom"
    value: "32GB/64GB/128GB"
  - id: "ram"
    value: "2GB/3GB/4GB"
  - id: "android"
    value: "Android 10/MIUI 12"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "IPS LCD, 400 nits (typ) 6.53 inches 720 x 1600 pixels, 20:9 ratio (~269 ppi density)"
  - id: "arch"
    value: "armhf"
  - id: "rearCamera"
    value: "13MP(wide), 2MP(macro), 2MP(depth)"
  - id: "frontCamera"
    value: "5MP"
  - id: "dimensions"
    value: "164.9 x 77 x 9 mm (6.49 x 3.03 x 0.35 in)"
  - id: "weight"
    value: "196 g (6.91 oz)"
  - id: "releaseDate"
    value: "August 20 2020"

contributors:
  - name: TheKit
    forum: "https://forums.ubports.com/user/thekit"
    photo: ""
  - name: TheVancedGamer
    forum: ""
    photo: ""
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
  - name: "Device Support"
    link: "https://t.me/ut_angelica"
  - name: "Report a bug"
    link: "https://gitlab.com/groups/ubports/community-ports/android10/xiaomi-redmi-9c/-/issues"
  - name: "Device Source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-9c"
  - name: "Kernel source"
    link: "https://gitlab.com/ubports/community-ports/android10/xiaomi-redmi-9c/kernel-xiaomi-mt6765"
---

---
name: "Asus Zenfone Max Pro M1"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/X00TD.png"

deviceInfo:
  - id: "cpu"
    value: "Quad-core 1.8 GHz Kryo 260 Gold + Quad-core 1.6 GHz Kryo 260 Silver"
  - id: "chipset"
    value: "Qualcomm SDM636 Snapdragon 636"
  - id: "gpu"
    value: "430 MHz Adreno 509"
  - id: "rom"
    value: "32 GB (3 GB) / 64 GB (4/6 GB) eMCP"
  - id: "ram"
    value: "3/4/6 GB RAM (LPDDR4X 1333 MHz dual-channel)"
  - id: "android"
    value: "Android 8.1.0"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "2160 x 1080 (18:9), 5.99 inch, 450 nits brightness, 1,500:1 contrast ratio"
  - id: "rearCamera"
    value: "13 MP f/2.2 (3/4 GB) / 16 MP f/2.0 (6 GB), 1.12-micron pixels, LED flash"
  - id: "frontCamera"
    value: "8 MP (3/4 GB) / 16 MP (6 GB), 1-micron pixels, f/2.2, Selfie-light"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "159 x 76 x 8.5 mm (6.26 x 2.99 x 0.33 in)"
  - id: "weight"
    value: "180 g (6.34 oz)"
externalLinks:
  - name: "Device Repository"
    link: "https://gitlab.com/ubports/community-ports/android9/asus-zenfone-max-pro-m1/asus-x00td"
  - name: "Kernel Repository"
    link: "https://gitlab.com/ubports/community-ports/android9/asus-zenfone-max-pro-m1/kernel-asus-sdm660"
  - name: "CI Builds"
    link: "https://gitlab.com/ubports/community-ports/android9/asus-zenfone-max-pro-m1/asus-x00td/-/pipelines"
  - name: "Telegram Group"
    link: "https://t.me/Halium_X00TDs"
contributors:
  - name: "iAboothahir"
    forum: ""
    photo: ""
---
